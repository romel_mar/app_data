json.extract! nosotro, :id, :nombre, :descripcion, :archivos, :created_at, :updated_at
json.url nosotro_url(nosotro, format: :json)
