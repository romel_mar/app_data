json.array!(@eventos) do |event|
  json.extract! event, :id
  json.title event.titulo
  json.description event.descripcion
  json.start event.fecha_inicio
  json.end event.fecha_fin
  json.url evento_url(event, format: :html)
end