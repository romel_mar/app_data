json.extract! galerium, :id, :fecha, :imagen, :created_at, :updated_at
json.url galerium_url(galerium, format: :json)
