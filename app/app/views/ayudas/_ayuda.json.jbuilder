json.extract! ayuda, :id, :nombre, :descripcion, :imagen, :category_id, :created_at, :updated_at
json.url ayuda_url(ayuda, format: :json)
