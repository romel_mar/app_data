class FacturaMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.factura_mailer.factura_yo.subject
#  attr_accessor :nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto

  def factura_yo(message)
    @nombre = message.nombre
    @apellidop = message.apellidop
    @apellidom = message.apellidom
    @email = message.email
    @domicilio = message.domicilio
    @razon_social = message.razon_social
    @rfc = message.rfc
    @pais = message.pais
    @estado = message.estado
    @municipio = message.municipio
    @fecha = message.fecha
    @monto = message.monto
    mail to: "centrodedesarrollovaronil@gmail.com", from: message.email
  end
end
