class ContactoMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contacto_mailer.contactame.subject
  #
  def contactame(message)
    @nombre = message.nombre
    @email = message.email
    @body = message.descripcion
    mail to: "centrodedesarrollovaronil@gmail.com", from: message.email
  end
end
