class ApplicationMailer < ActionMailer::Base
  default from: 'centrodedesarrollovaronil@gmail.com'
  layout 'mailer'
end
