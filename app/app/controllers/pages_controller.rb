class PagesController < ApplicationController
  def inicio
    @mision = Nosotro.where(id:'1')
    @vision = Nosotro.where(id:'2')
    @valores = Nosotro.where(id:'3')
    @historia = Nosotro.where(id:'4')
    @video = Nosotro.where(id:'5')
    @transparencia = Nosotro.where(id:'6')
    @imagenesCarrusel = Carrusel.all
    
  end

  def nosotros
  end
  
  def programas
      @programas = Programa.all
      @total_programas = Programa.count
  end

  def eventos_noticias
    @noticias = Noticia.order(created_at: :desc)
  end

  def como_ayudar
    @total_categorias = Category.count
    @categorias = Category.all
    #@ayudas = Ayuda.where(categoria_id: @categoria_id).first
  end

  def galeria
    @fechas = Galerium.select(:fecha).map(&:fecha).uniq
    @galeria = Galerium.all
    
  end
  
  def logos
    @logos = Logo.all
  end
  
  def aviso
    @aviso = Nosotro.where(id:'7')
  end
end
