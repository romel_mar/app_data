class FacturasController < ApplicationController
    def new
        @factura = Factura.new
    end
    def create
    message_params = params.require(:factura).permit(:nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto)
    @message = Factura.new message_params
    
    if @message.valid?
      FacturaMailer.factura_yo(@message).deliver_now
      redirect_to new_factura_url, notice: "Su factura ha sido enviada"
    else
      render :new
    end
    end
end
