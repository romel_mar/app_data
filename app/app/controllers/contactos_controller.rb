class ContactosController < ApplicationController
  def new
    @contacto = Contacto.new
  end
  
  def create
    message_params = params.require(:contacto).permit(:nombre, :email, :descripcion)
    @message = Contacto.new message_params

    if @message.valid?
      ContactoMailer.contactame(@message).deliver_now
      redirect_to new_contacto_url, notice: "Su mensaje ha sido enviado"
    else
      render :new
    end
  end
end

