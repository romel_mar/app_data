//= require materialize

//= require jquery
//= require jquery_ujs

 
// Sempre inclua
//= require turbolinks
//= require materialize-sprockets
//= require_tree .
//= requiere leaflet
//= require light-gallery
//= require moment 
//= require fullcalendar
//= require fullcalendar/locale-all

/* Function to load fullCalendar */
function eventCalendar() {
  return $('.calendar').fullCalendar({ 
    events: '/eventos.json',
    locale: 'es'
  });
};

/* Function to remove fullCalendar from a before_cache tag */
function clearCalendar() {
  $('.calendar').fullCalendar('delete'); // In case delete doesn't work.
  $('.calendar').html('');
};

var play = false;
$(document).on('turbolinks:load', function(){
  //Start carousel
  $('.carousel.carousel-slider').carousel({
    fullWidth: true
  }).css('height', 600);
  
  if (play == false){
     autoplay(); 
  }
  function autoplay() {
      $('.carousel').carousel('next');
      setTimeout(autoplay, 5000);
      play = true;
  }
  
  // move next carousel
   $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });
  
  
  $('.modal').modal();
  
  $(".modal-close").each(function(){
    M.Modal._count = 0;
  });
  
  
  $(".button-collapse").sideNav();

  $(".maps").each(function(){
    var mapID = $(this).attr("id");
    
    var mymap = L.map(mapID).setView([20.636959, -100.3229440], 15);
  
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZ2lsLW1vcmFsZXMxIiwiYSI6ImNqZTY2djUyZTVsOHUycHQzZ28yNDQ0MmUifQ.ijm5xeeeWcqYq5HJkk0kBw'
    }).addTo(mymap);
    
    var marker = L.marker([20.636959, -100.3229440]).addTo(mymap);
    
    marker.bindPopup("<b style='margin-left:30px;'>Mineral de pozos</b><br>El Pozo, El Marques Querétaro").openPopup();
  });

  /* Call fullCalendar loader function */
  eventCalendar();

   var gallery = $('.lightGallery').lightGallery();
});

/* Call fullCalendar remover function */
$(document).on('turbolinks:before-cache', clearCalendar);
