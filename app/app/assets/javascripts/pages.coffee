# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# $(document).ready -> 
#     mymap = L.map('mapid').setView([20.636959, -100.3229440], 15)

#     L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
#         attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
#         maxZoom: 18,
#         id: 'mapbox.streets',
#         accessToken: 'pk.eyJ1IjoiZ2lsLW1vcmFsZXMxIiwiYSI6ImNqZTY2djUyZTVsOHUycHQzZ28yNDQ0MmUifQ.ijm5xeeeWcqYq5HJkk0kBw'
#     }).addTo(mymap);
    
#     marker = L.marker([20.636959, -100.3229440]).addTo(mymap);
    
#     marker.bindPopup("<b style='margin-left:30px;'>Mineral de pozos</b><br>El Pozo, El Marques Querétaro").openPopup();