class Factura
    include ActiveModel::Model
    attr_accessor :nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto
    validates :nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto, :presence => true
    validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true
end

