class Nosotro < ApplicationRecord
    mount_uploader :archivos, AttachmentUploader
    
    validates :nombre, presence: true, allow_blank: false
    #VALID_VIDEO = /https\:\/\/www.youtube.com\/watch\?v=[a-zA-z1-9\-]+/
    validates :descripcion, presence: true, allow_blank: false#, format: { with: VALID_VIDEO }
end