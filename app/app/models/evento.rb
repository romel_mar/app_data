class Evento < ApplicationRecord
    mount_uploader :imagen, ImageUploader 
    
    validates :titulo, presence: true
    validates :descripcion, presence: true
    validates :imagen, presence: true
    validates :fecha_inicio, presence: true
    validates :fecha_fin, presence: true
    validate :check_end_date
    
    # Validate that end date is not before end date
    def check_end_date
        if fecha_inicio > fecha_fin
          errors.add(:fecha_fin, "Ingrese fecha igual o posterior a la fecha de inicio")
        end
    end
end
