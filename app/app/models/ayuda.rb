class Ayuda < ApplicationRecord
    belongs_to :category
    
    mount_uploader :imagen, ImageUploader
    
    validates :nombre, presence: true
    validates :descripcion, presence: true
    validates :imagen, presence: true
    
    def name
        nombre
    end
end
