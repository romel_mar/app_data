class Carrusel < ApplicationRecord
    mount_uploader :imagen, ImageUploader
    validates :imagen, presence: true
    
    def name
        imagen
    end
end
