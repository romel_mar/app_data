class Contacto
  include ActiveModel::Model
  attr_accessor :nombre, :email, :descripcion
  validates :nombre, :email, :descripcion, presence: true
  validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true
end