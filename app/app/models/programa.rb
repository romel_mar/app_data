class Programa < ApplicationRecord
    has_many :foto, dependent: :destroy
    
    validates :titulo, presence: true
    validates :descripcion, presence: true

end
