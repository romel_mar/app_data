class Noticia < ApplicationRecord
    mount_uploader :imagen, ImageUploader 
    
    validates :titulo, presence: true, allow_blank: false
    validates :descripcion, presence: true, allow_blank: false
    validates :imagen, presence: true,  length: {minimum: 5}, allow_blank: false

end
