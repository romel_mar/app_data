class Galerium < ApplicationRecord
    validates :fecha, presence: true
    validates :fecha, numericality: { only_integer: true }
    validates :fecha, numericality: { greater_than: 2000 }
    validates :fecha, numericality: { less_than: 2050 }
    validates :imagen, presence: true
    mount_uploader :imagen, ImageUploader
end
