class Foto < ApplicationRecord
    validates :nombre, presence: true
    validates :imagen, presence: true
    mount_uploader :imagen, ImageUploader
    
    def name
       nombre 
    end
end
