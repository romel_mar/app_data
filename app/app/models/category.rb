class Category < ApplicationRecord
    has_many :ayudas, dependent: :destroy
    validates :nombre, presence: true
    
    def name
        nombre
    end
end
