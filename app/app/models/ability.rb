class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    # user ||= User.new # guest user (not logged in)
    # can :manage, :all
    # can :access, :rails_admin       # only allow admin users to access Rails Admin
    # can :dashboard                  # allow access to dashboard
    if user.admin?
            can :manage, :all
            can :access, :rails_admin       # only allow admin users to access Rails Admin
            can :dashboard                  # allow access to dashboard
    end
    if user.noticias?
          can :manage, Noticia
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    if user.nosotros?
          can :read, Nosotro
          can :edit, Nosotro
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end 
    if user.fotos?
          can :manage, Foto
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end 
    if user.programas?
          can :manage, Programa
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    if user.ayuda?
          can :manage, Ayuda
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    if user.categoria?
          can :manage, Category
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    if user.logo?
          can :manage, Logo
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    if user.carrusel?
          can :manage, Carrusel
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    
    if user.eventos?
          can :manage, Evento
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    
    if user.galeria?
          can :manage, Galerium
          can :access, :rails_admin       # only allow admin users to access Rails Admin
          can :dashboard, :all                  # allow access to dashboard
    end
    
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
