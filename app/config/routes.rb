Rails.application.routes.draw do
  resources :ayudas
  resources :categories
  resources :galeria
  resources :fotos
  resources :programas
  resources :eventos
  resources :logos
  resources :carrusels
  resources :programas
  resources :nosotros
  resources :noticias
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
 
  get "inicio", to: "pages#inicio"
  
  get "nosotros", to: "pages#nosotros"

  get "programas_sociales", to: "pages#programas"

  get "eventos_noticias", to: "pages#eventos_noticias"

  get "como_ayudar", to: "pages#como_ayudar"

  get "fotos_galeria", to: "pages#galeria"
  
  get "factura", to: 'facturas#new', as: 'new_factura'
  
  post "factura", to: 'facturas#create', as: 'create_factura'
  
  get 'contacto', to: 'contactos#new', as: 'new_contacto'
  post 'contacto', to: 'contactos#create', as: 'create_contacto'
  
  root 'pages#inicio'

  devise_for :users, controllers: {
      sessions: 'devise/sessions',
      passwords: 'devise/passwords',
      registrations: 'devise/registrations'
  }

end
