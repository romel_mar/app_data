RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
     warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      except ['Nosotro'] 
    end
    export
    bulk_delete
    show
    edit
    delete do
      except ['Nosotro'] 
    end
    #show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
  
  config.model 'User' do
    create do
      configure :current_sign_in_at do
        hide
      end
      configure :sign_in_count do
        hide
      end
      configure :last_sign_in_at do
        hide
      end
      configure :current_sign_in_ip do
        hide
      end
      configure :last_sign_in_ip do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :reset_password_sent_at do
        hide
      end
    end
    list do
      configure :current_sign_in_at do
        hide
      end
      configure :sign_in_count do
        hide
      end
      configure :last_sign_in_at do
        hide
      end
      configure :current_sign_in_ip do
        hide
      end
      configure :last_sign_in_ip do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :reset_password_sent_at do
        hide
      end
    end
    edit do
      configure :current_sign_in_at do
        hide
      end
      configure :sign_in_count do
        hide
      end
      configure :last_sign_in_at do
        hide
      end
      configure :current_sign_in_ip do
        hide
      end
      configure :last_sign_in_ip do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :remember_created_at do
        hide
      end
      configure :reset_password_sent_at do
        hide
      end
    end
  end
  
  config.model 'Ayuda' do
    label I18n.t('.ayuda_label')
    label_plural I18n.t('.ayuda_label_plural')
    
    list do
      configure :id do
        hide
      end
    end
  end
  
  config.model 'Carrusel' do
    label I18n.t('.carrusel_label')
    label_plural I18n.t('.carrusel_label_plural')
  end
  
  config.model 'Category' do
    label I18n.t('.category_label')
    label_plural I18n.t('.category_label_plural')
    edit do
      field :nombre
    end
  end
  
  config.model 'Logo' do
    label I18n.t('.logo_label')
    label_plural I18n.t('.logo_label_plural')
  end
  
  config.model 'Nosotro' do
    label I18n.t('.nosotros_label')
    label_plural I18n.t('.nosotros_label_plural')
  end
  
  config.model 'Noticia' do
    label I18n.t('.noticia_label')
    label_plural I18n.t('.noticia_label_plural')
  end
  
  config.model 'Photo' do
    label I18n.t('.foto_label')
    label_plural I18n.t('.foto_label_plural')
    edit do
      field :image
    end
  end
  
  config.model 'Programa' do
    label I18n.t('.programa_label')
    label_plural I18n.t('.programa_label_plural')
  end
  
  config.model 'Foto' do
    label I18n.t('.foto_label')
    label_plural I18n.t('.foto_label_plural')
    list do
      configure :programa_id do
        hide
      end
    end
    edit do
      field :nombre
      field :imagen
    end
  end
  
  config.model 'Galerium' do
    label I18n.t('.galerium_label')
    label_plural I18n.t('.galerium_label_plural')
  end
  
  config.model 'User' do
    label I18n.t('.usuario_label')
    label_plural I18n.t('.usuario_label_plural')
  end

  config.model 'Evento' do
    edit do
      field :titulo
      field :descripcion
      field :imagen
      # Config to keep same format for dates in Spanish and English
      field :fecha_inicio do
        strftime_format '%d-%m-%Y %H:%M:%S'
      end
      field :fecha_fin do
        strftime_format '%d-%m-%Y %H:%M:%S'
      end
    end
  end
  
  config.model 'Galerium' do
    label I18n.t('.galeria_label')
    label_plural I18n.t('.galeria_label_plural')
  end
end
