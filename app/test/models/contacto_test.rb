require 'test_helper'

class ContactoTest < ActiveSupport::TestCase
  test 'responds to name, email and body' do 
    msg = Contacto.new

    assert msg.respond_to?(:nombre),  'does not respond to :name'
    assert msg.respond_to?(:email), 'does not respond to :email'
    assert msg.respond_to?(:descripcion),  'does not respond to :body'
  end
  
  test 'should be valid when all attributes are set' do
    attrs = { 
      nombre: 'stephen',
      email: 'stephen@example.org',
      descripcion: 'kthnxbai'
    }

    msg = Contacto.new attrs
      assert msg.valid?, 'should be valid'
  end
  
  test 'nombre, email and descripcion are required by law' do
    msg = Contacto.new

    refute msg.valid?, 'Blank Mesage should be invalid'

    assert_match " ", msg.errors[:nombre].to_s
    assert_match " ", msg.errors[:email].to_s
    assert_match " ", msg.errors[:descripcion].to_s
  end
end