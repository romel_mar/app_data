require 'test_helper'

class NoticiaTest < ActiveSupport::TestCase
  def setup
    @noticia = Noticia.new(titulo: "Título de la noticia", 
                          descripcion: "Esta es la descripcion de la noticia", 
                          imagen: "/imagenes/noticia.jpg")
  end
  
  test "titulo should be present" do
    @noticia.titulo = ""
    assert_not @noticia.valid?
  end
  
  test "descripcion should be present" do
    @noticia.descripcion = ""
    assert_not @noticia.valid?
  end
  
  test "imagen should be present" do
    @noticia.imagen = ""
    assert_not @noticia.valid?
  end
  
end
