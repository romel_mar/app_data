require 'test_helper'

class NosotroTest < ActiveSupport::TestCase
  
  #Test US17 Edit vision section
  test "Vision name and description are presented (no blank)" do
    vision = nosotros(:vision)
    vision.nombre = "Vision"
    vision.descripcion = "Nuestra vision es..."
    assert vision.valid?, "Hay campos vacios"
  end

  #Test US25 Upload Youtube video
  test "youtube video is presented (no blank)" do
    video = nosotros(:video)
    video.nombre = "Video"
    video.descripcion = "https://www.youtube.com/watch?v=rW-j2lGE1i4"
    assert video.valid?, "Hay campos vacios"
  end

  #Test US48 Edit mision section
  test "Mision name and description are presented (no blank)" do
    mision = nosotros(:mision)
    mision.nombre = "Mision"
    mision.descripcion = "Nuestra mision es..."
    assert mision.valid?, "Hay campos vacios"
  end

  #Test US49 Edit values section
  test "Valores name and description are presented (no blank)" do
    valores = nosotros(:valores)
    valores.nombre = "Valores"
    valores.descripcion = "Nuestros valores son..."
    assert valores.valid?, "Hay campos vacios"
  end
  
  test "Transparency name and description are presented (no blank)" do
    transparencia = nosotros(:transparency)
    transparencia.nombre = "Transparencia"
    transparencia.descripcion = "Este es el archivo de transparencia..."
    assert transparencia.valid?, "Hay campos vacios"
  end
  
 #Test US61 Update Notice of privacy
  test "Notice of privacy name and description are presented (no blank)" do
    privacy = nosotros(:privacy)
    privacy.nombre = "Aviso de privacidad"
    privacy.descripcion = "Este es el aviso de privacidad..."
    privacy.archivos = "aviso.pdf"
    assert privacy.valid?, "Hay campos vacios"
  end  
end
