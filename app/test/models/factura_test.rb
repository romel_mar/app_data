require 'test_helper'

class FacturaTest < ActiveSupport::TestCase
=begin  
  test 'responds to all the fields' do 
    msg = Factura.new
    #  attr_accessor :nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto
    assert msg.respond_to?(:nombre),  'No responde a :nombre'
    assert msg.respond_to?(:apellidop), 'No responde a :apellidop'
    assert msg.respond_to?(:apellidom),  'No responde a :apellidom'
    assert msg.respond_to?(:email),  'No responde a :email'
    assert msg.respond_to?(:domicilio),  'No responde a :domicilio'
    assert msg.respond_to?(:razon_social),  'No responde a :razon_social'
    assert msg.respond_to?(:rfc),  'No responde a :rfc'
    assert msg.respond_to?(:pais),  'No responde a :pais'
    assert msg.respond_to?(:estado),  'No responde a :estado'
    assert msg.respond_to?(:municipio),  'No responde a :municipio'
    assert msg.respond_to?(:fecha),  'No responde a :fecha'
    assert msg.respond_to?(:monto),  'No responde a :monto'
  end
  
  # previous test omitted

  test 'Deben de ser validos los campos' do
    attrs = { 
      nombre: 'romel',
      apellidop: 'martinez',
      apellidom: 'olvera',
      email: 'romel@hotmail.com',
      domicilio: 'pedregal',
      razon_social: 'amor',
      rfc: 'RMAO29WUIEO',
      pais: 'México',
      estado: 'Quéretaro',
      municipio: 'Quéretaro',
      fecha: '18/04/2018',
      monto: '4000'
    }

    msg = Factura.new attrs
    assert msg.valid?, 'should be valid'
  end
  
  test 'name, email and body are required by law' do
    msg = Factura.new

    refute msg.valid?, 'Blank Mesage should be invalid'

    assert_match " ", msg.errors[:nombre].to_s
    assert_match " ", msg.errors[:apellidop].to_s
    assert_match " ", msg.errors[:apellidom].to_s
    assert_match " ", msg.errors[:email].to_s
    assert_match " ", msg.errors[:domicilio].to_s
    assert_match " ", msg.errors[:razon_social].to_s
    assert_match " ", msg.errors[:rfc].to_s
    assert_match " ", msg.errors[:pais].to_s
    assert_match " ", msg.errors[:estado].to_s
    assert_match " ", msg.errors[:municipio].to_s
    assert_match " ", msg.errors[:fecha].to_s
    assert_match " ", msg.errors[:monto].to_s
  end
=end
end
