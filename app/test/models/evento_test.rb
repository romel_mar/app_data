require 'test_helper'

class EventoTest < ActiveSupport::TestCase
  def setup
    @evento = Evento.new(titulo: "Título del evento", 
                          descripcion: "Esta es la descripcion del evento", 
                          imagen: "/imagenes/noticia.jpg",
                          fecha_inicio: "15-06-2018 10:20:00",
                          fecha_fin: "16-06-2018 11:00:00")
  end
  
  test "titulo should be present" do
    @evento.titulo = ""
    assert_not @evento.valid?
  end
  
  test "descripcion should be present" do
    @evento.descripcion = ""
    assert_not @evento.valid?
  end
  
  test "imagen should be present" do
    @evento.imagen = ""
    assert_not @evento.valid?
  end
  
end
