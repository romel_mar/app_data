require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  
  #Test US## Register categoria
  test "should add categoria" do
    nombre = "Voluntariado"
    assert_difference('Category.count', 1, "No se agrego la categoria") do
      post categories_url, params: { category: { nombre: nombre } }
    end
    categoria = Category.last
    assert_equal categoria.nombre, nombre, "El nombre de categoria no se guardo"
  end

  #Test US## Consult categoria
  test "should show categoria" do
    categoria = categories(:especie)
    assert_raises(NameError, "La categoria no esta registrada") do
      get category_url(categoria)
      assert_not_response :success
    end
  end

  #Test US## Update categoria
  test "should update categoria" do
    categoria = categories(:efectivo)
    categoria_id = categoria.id
    ayudas = Ayuda.where(category_id: categoria_id)
    nombre = "Voluntariado"
    patch category_url(categoria), params: { category: { nombre: nombre } }
    categoria.reload
    assert_equal categoria.nombre, nombre, "El nombre de categoria no se actualizo"
    ayudas.each do |ayuda|
        assert_equal ayuda.category_id, categoria.id, "La ayuda no se actualizo"
    end
  end

  #Test US## Delete categoria
  test "should destroy categoria" do
    categoria = categories(:efectivo)
    categoria_id = categoria.id
    categoria_id2 = categories(:especie).id
    ayudas = Ayuda.where(category_id: categoria_id)
    
    assert_difference('Category.count', -1, "No se elimino la categoria") do
      delete category_url(categoria)
    end
    
    assert_raises(Exception, "No se elimino la categoria") do
      assert_not_nil Category.find(categoria_id)
    end
    #Comprobar que se eliminaron las ayudas relacionadas
    assert_raises(Exception, "No se elimino la ayuda relacionada") do
        assert_nil Ayuda.where(category_id: categoria_id)
    end
  end

end
