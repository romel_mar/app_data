require 'test_helper'

class GaleriaControllerTest < ActionDispatch::IntegrationTest

  setup do
    @galerium = galeria(:one)
  end

  test "should get index" do
    get galeria_url
    assert_response :success
  end

  test "should get new" do
    get new_galerium_url
    assert_response :success
  end

#Test US23 Add photos to gallery
  test "should create photo gallery" do
    nombre="apple.png"
    fecha="2014"
    imagen= fixture_file_upload "apple.png"
    assert_difference('Galerium.count',1,"No se agrego la foto a la galeria") do
      post galeria_url, params: { galerium: { fecha: fecha, imagen: imagen } }
    end
    
    nueva_foto = Galerium.last
    path = nueva_foto.imagen.url(:thumb).split('thumb_')
    assert_equal nombre, path.at(1), "La foto no se subio correctamente"
  end

#Test US51 and US65 Consult social programs
  test "should show gallery" do
    foto =galeria(:one)
    assert_raises(NameError, "La foto no esta registrada") do
      get galerium_url(foto)
      assert_not_response :success
    end
  end

  test "should get edit" do
    get edit_galerium_url(@galerium)
    assert_response :success
  end


#Test US24 Delete photos to gallery
  test "should destroy photo gallery" do
    foto = galeria(:one)
    foto_id= foto.id
    
    assert_difference('Galerium.count', -1,"No se elimino la foto") do
      delete galerium_url(foto)
    end

    assert_raises(Exception, "No se elimino la foto") do
      assert_not_nil Galerium.find(foto_id)
    end
  end
end
