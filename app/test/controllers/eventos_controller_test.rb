require 'test_helper'

class EventosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @evento = eventos(:one)
  end
  
  test "should create evento" do
    nombre_imagen = 'evento.jpg'
    imagen = fixture_file_upload nombre_imagen
    assert_difference('Evento.count', 1, "No se creó el evento") do
      post eventos_url, params: { evento: { titulo: @evento.titulo, 
                                            descripcion: @evento.descripcion, 
                                            fecha_inicio: @evento.fecha_inicio, 
                                            fecha_fin: @evento.fecha_fin, 
                                            imagen: imagen } }
    end
    
    evento = Evento.last
    assert_equal evento.titulo, @evento.titulo, "El título del evento no se guardó"
    assert_equal evento.descripcion, @evento.descripcion, "La descripcion del evento no se guardó"
    assert_equal evento.fecha_inicio, @evento.fecha_inicio, "La fecha de inicio no se guardó"
    assert_equal evento.fecha_fin, @evento.fecha_fin, "La fecha de fin no se guardó"
    image_path = evento.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, image_path.at(1), "La imagen no se guardó"
  end

  test "should show evento" do
    assert_raises(NameError, "El evento no está registrado") do
      get evento_url(@evento)
      assert_not_response :success
    end
  end

  test "should update evento" do
    new_titulo = 'Título editado'
    new_descripcion = 'Descripción de evento editado'
    new_fechainicio = '2018-06-15 02:33:34'
    new_fechafin = '2018-06-16 10:00:00'
    nombre_imagen = 'evento.jpg'
    imagen = fixture_file_upload nombre_imagen
    patch evento_url(@evento), params: { evento: { titulo: new_titulo, 
                                                      descripcion: new_descripcion,
                                                      imagen: imagen,
                                                      fecha_inicio: new_fechainicio, 
                                                      fecha_fin: new_fechafin
                                                  } }
    @evento.reload
    assert_equal @evento.titulo, new_titulo, "El título del evento no se actualizó"
    assert_equal @evento.descripcion, new_descripcion, "La descripcion del evento no se actualizó"
    assert_equal @evento.fecha_inicio, new_fechainicio, "La fecha de inicio no se actualizó"
    assert_equal @evento.fecha_fin, new_fechafin, "La fecha de fin no se actualizó"
    image_path = @evento.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, image_path.at(1), "La imagen no se guardó"
  end

  test "should destroy evento" do
    evento_id = @evento.id
    #evento_id_fail = eventos(:two).id
    
    assert_difference('Evento.count', -1, "No se eliminó el evento") do
      delete evento_url(@evento)
    end
    
    assert_raises(Exception, "No se eliminó el evento") do
      assert_not_nil Evento.find(evento_id)
    end
  end
  
end
