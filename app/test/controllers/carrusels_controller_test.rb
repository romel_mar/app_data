require 'test_helper'

class CarruselsControllerTest < ActionDispatch::IntegrationTest
  
  #Test US12 Add photographs to the carrusel
  test "should add photograph to carrusel table" do
    nombre_imagen = 'apple.png'
    imagen = fixture_file_upload nombre_imagen
    assert_difference('Carrusel.count', 1, "No se agrego imagen a la tabla carrusel") do
      post carrusels_url, params: { carrusel: { imagen: imagen} }
    end
    nueva_foto = Carrusel.last
    path = nueva_foto.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, path.at(1), "La foto no se subio correctamente"
  end

  #Test US13 Delete photographs from carrusel
  test "should delete photograph from carrusel" do
    imagen = carrusels(:imagen_1)
    imagen_id = imagen.id
    imagen_id2 = carrusels(:imagen_2).id
    assert_difference('Carrusel.count', -1, "No se elimino foto de carrusel") do
      delete carrusel_url(imagen)
    end
    assert_raises(Exception, "No se elimino la foto del carrusel") do
      assert_not_nil Carrusel.find(imagen_id)
    end
  end  

  #Test US14 Update photograph from the carrusel
  test "should update photograph in the carrusel" do
    imagen = carrusels(:imagen_1)
    nombre_imagen = 'apple.png'
    nueva_imagen = fixture_file_upload nombre_imagen
    patch carrusel_url(imagen), params: { carrusel: { imagen: nueva_imagen } }
    imagen.reload
    path = imagen.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, path.at(1),  "La foto del carrusel no se actualizo"
  end

end
