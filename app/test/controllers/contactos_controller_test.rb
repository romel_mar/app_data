
require 'test_helper'

class ContactosControllerTest < ActionDispatch::IntegrationTest
=begin
  test "GET new" do
    get new_contacto_url

    assert_response :success

    assert_select 'form' do 
      assert_select 'input[type=text]'
      assert_select 'input[type=email]'
      assert_select 'textarea'
      assert_select 'input[type=submit]'
    end
  end
end

  test "POST create" do
    assert_difference 'ActionMailer::Base.deliveries.size', 1 do
      post create_contacto_url, params: {
        contacto: {
          nombre: 'Brenda',
          email: 'brenda_fparada@hotmail.com',
          descripcion: 'hola'
        }
      }
    end

    assert_redirected_to new_contacto_url

    follow_redirect!
=end  
end
