require 'test_helper'

class NosotrosControllerTest < ActionDispatch::IntegrationTest
  
  #Test US17 Edit vision section
  test "should update vision section" do
    vision = nosotros(:vision)
    nombre = "Vision"
    descripcion = "Nuestra vision"
    patch nosotro_url(vision), params: { nosotro: { nombre: nombre, descripcion: descripcion } }
    vision.reload
    assert vision.valid?
    assert_equal nombre,  "Vision", "El nombre debe ser forzosamente Vision"
    assert_equal nombre, vision.nombre,  "El  nombre de vision no se actualizo"
    assert_equal descripcion, vision.descripcion, "La descripcion de vision no se actualizo"
  end

  #Test US25 Upload Youtube video
  test "should upload youtube video" do
    video = nosotros(:video)
    nombre = "Video"
    descripcion = "https://www.youtube.com/watch?v=VxNllYdEleQ"
    patch nosotro_url(video), params: { nosotro: { nombre: nombre, descripcion: descripcion } }
    video.reload
    assert video.valid?
    assert_equal nombre, video.nombre,  "El  nombre de video no se actualizo"
    assert_equal descripcion, video.descripcion, "La descripcion de video no se actualizo"
  end

  #Test US48 Edit mision section
  test "should update mision section" do
    mision = nosotros(:mision)
    nombre = "Mision"
    descripcion = "Nuestra mision"
    patch nosotro_url(mision), params: { nosotro: { nombre: nombre, descripcion: descripcion } }
    mision.reload
    assert mision.valid?
    assert_equal nombre, mision.nombre,  "El  nombre de mision no se actualizo"
    assert_equal descripcion, mision.descripcion, "La descripcion de mision no se actualizo"
  end

  #Test US49 Edit values section
  test "should update values section" do
    valores = nosotros(:valores)
    nombre = "Valores"
    descripcion = "Nuestros valores"
    patch nosotro_url(valores), params: { nosotro: { nombre: nombre, descripcion: descripcion } }
    valores.reload
    assert valores.valid?
    assert_equal nombre, valores.nombre,  "El  nombre de valores no se actualizo"
    assert_equal descripcion, valores.descripcion, "La descripcion de valores no se actualizo"
  end

  #Test US61 Consult Notice of privacy
  test "should update aviso" do
    aviso=nosotros(:privacy)
    nombre= "aviso.pdf"
    archivos = fixture_file_upload nombre
    descripcion = "descripcion actualizada"
    patch nosotro_url(aviso), params: { nosotro: { archivos: archivos, descripcion: descripcion } }
    aviso.reload
    assert_equal aviso.descripcion, descripcion, "La descripcion no se actualizo"
    
    path = aviso.archivos.url.split('/')
    assert_equal path.at(5),nombre, "El archivo no se subio correctamente"
    
    get nosotro_url(aviso)
    assert_response :success
    
  end
  
  test "Should upload Transparency file" do
    transparencia = nosotros(:transparency)
    nombre = "Transparencia"
    descripcion = "Descripcion actualizada"
    file_name = 'transparencia.pdf'
    archivos = fixture_file_upload file_name
    
    patch nosotro_url(transparencia), params: { nosotro: { nombre: nombre, descripcion: descripcion, archivos: archivos } }
    transparencia.reload
    assert_equal transparencia.descripcion, descripcion, "La descripcion se actualizo"
    
    path = transparencia.archivos.url.split('/')
    assert_equal path.at(5), file_name, "El archivo se subió correctamente"
  end
  
  test "should delete Transparency file" do
    transparencia = nosotros(:transparency)
    trans_id = nosotros(:transparency).id
    #fail = noticias(:one)
    
    assert_difference('Nosotro.count', -1, "No se eliminó transparencia") do
      delete nosotro_url(transparencia)
    end
    
    assert_raises(Exception, "No se eliminó transparencia") do
      assert_not_nil Nosotro.find(trans_id)
    end
  end

end
