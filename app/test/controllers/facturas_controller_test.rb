require 'test_helper'

class FacturasControllerTest < ActionDispatch::IntegrationTest
=begin  
  test "GET new" do
    get new_factura_url

    assert_response :success
    #  attr_accessor :nombre,:apellidop, :apellidom, :email, :domicilio, :razon_social, :rfc, :pais, :estado, :municipio, :fecha, :monto
    assert_select 'form' do 
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=email]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'input[type=number]'
      assert_select 'input[type=submit]'
    end
  end
  
  test "POST create" do
    assert_difference 'ActionMailer::Base.deliveries.size', 1 do
      post create_factura_url, params: {
        factura: {
          nombre: 'romel',
          apellidop: 'martinez',
          apellidom: 'olvera',
          email: 'romel@hotmail.com',
          domicilio: 'pedregal',
          razon_social: 'amor',
          rfc: 'RMAO29WUIEO',
          pais: 'México',
          estado: 'Quéretaro',
          municipio: 'Quéretaro',
          fecha: '18/04/2018',
          monto: '4000'
        }
      }
    end

    assert_redirected_to new_factura_url

    follow_redirect!

    #assert_match /Message received, thanks!/, response.body
  end
  
=begin  
  test "invalid POST create" do
    post create_factura_url, params: {
      message: { nombre: '',apellidop: '', apellidom:'', email: '', domicilio: '', razon_social: '', rfc: '', pais: '', estado: '', municipio: '', fecha: '', monto: ''}
    }
    
    assert_match 'Nombre. ', response.body
    assert_match 'Apellidop. ', response.body
    assert_match 'Apellidom. ', response.body
    assert_match 'Email. ', response.body
    assert_match 'Domicilio. ', response.body
    assert_match 'Razon_social. ', response.body
    assert_match 'Rfc. ', response.body
    assert_match 'Pais. ', response.body
    assert_match 'Estado. ', response.body
    assert_match 'Municipio. ', response.body
    assert_match 'Fecha. ', response.body
    assert_match 'Monto. ', response.body
  end
=end
#https://www.murdo.ch/blog/build-a-contact-form-with-ruby-on-rails-part-3
end
