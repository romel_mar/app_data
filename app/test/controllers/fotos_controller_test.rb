require 'test_helper'

class FotosControllerTest < ActionDispatch::IntegrationTest
  
  setup do
    @foto = fotos(:one)
    @programa= programas(:programa)
  end

  test "should get index" do
    get fotos_url
    assert_response :success
  end

  test "should get new" do
    get new_foto_url
    assert_response :success
  end

#Test US32 Add photos to social programs
  test "should create foto" do
    nombre="apple.png"
    imagen= fixture_file_upload nombre
    programa_id=1
  
    raise "El programa no existe" if Programa.find(programa_id).nil?
    
    assert_difference('Foto.count',1,"No se agrego la foto de programas sociales") do
      post fotos_url, params: { foto: { imagen: imagen, nombre: nombre, programa_id: programa_id } }
    end
      
      nueva_foto = Foto.last
      path = nueva_foto.imagen.url(:thumb).split('thumb_')
      assert_equal nombre, path.at(1), "La foto no se subio correctamente"
      programa=nueva_foto.programa_id
      assert_equal programa_id, programa, "La foto no se agrego al programa"

  end

  test "should show foto" do
    get foto_url(@foto)
    assert_response :success
  end

  test "should get edit" do
    get edit_foto_url(@foto)
    assert_response :success
  end

#Test US35 Add update to social programs
  test "should update foto" do
    foto = fotos(:one)
    nombre="foto update"
    nombre_imagen="apple.png"
    imagen= fixture_file_upload nombre_imagen
    programa_id = @programa.id
    patch foto_url(foto), params: { foto: { imagen: imagen, nombre: nombre, programa_id: programa_id } }
    foto.reload
    
    assert_equal foto.nombre, nombre, "El nombre de la foto no se actualizo"
    nueva_foto = Foto.last
    path = nueva_foto.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, path.at(1), "La foto no se actualizo correctamente"
    programa=nueva_foto.programa_id
    assert_equal programa_id, programa, "La foto no se agrego al programa"
  end

#Test US36 Delete photos to social programs
  test "should destroy foto" do
    foto = fotos(:one)
    foto_id= foto.id
    
    assert_difference('Foto.count', -1,"No se elimino la foto") do
      delete foto_url(foto)
    end

    assert_raises(Exception, "No se elimino la foto") do
      assert_not_nil Foto.find(foto_id)
    end
  end
  
end
