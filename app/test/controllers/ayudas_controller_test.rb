require 'test_helper'

class AyudasControllerTest < ActionDispatch::IntegrationTest
  
  #Test US53 Register como ayudar
  test "should add como ayudar" do
    nombre = "Cuenta banco"
    descripcion = "El numero de cuenta es 00-00"
    nombre_img = 'apple.png'
    imagen = fixture_file_upload nombre_img
    categoria = categories(:efectivo).id
    assert_difference('Ayuda.count', 1, "No se agrego la ayuda") do
      post ayudas_url, params: { ayuda: { nombre: nombre, descripcion: descripcion, imagen: imagen, category_id: categoria } }
    end
    ayuda = Ayuda.last
    assert_equal ayuda.nombre, nombre, "El nombre de ayuda no se guardo"
    assert_equal ayuda.descripcion, descripcion, "La descripcion de ayuda no se guardo"
    path = ayuda.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_img, path.at(1), "La imagen no se guardo"
    assert_equal ayuda.category_id, categoria, "La categoria no se guardo"
  end

  #Test US54 Consult como ayudar
  test "should show ayuda" do
    ayuda = ayudas(:ayuda1)
    assert_raises(NameError, "La ayuda no esta registrada") do
      get ayuda_url(ayuda)
      assert_not_response :success
    end
  end

  #Test US55 Update como ayudar
  test "should update como ayudar" do
    ayuda = ayudas(:ayuda1)
    nombre = "Dinero"
    descripcion = "El numero de cuenta..."
    nombre_img = 'apple.png'
    imagen = fixture_file_upload nombre_img
    categoria = categories(:especie).id
    patch ayuda_url(ayuda), params: { ayuda: { nombre: nombre, descripcion: descripcion, imagen: imagen, category_id: categoria } }
    ayuda.reload
    assert_equal ayuda.nombre, nombre, "El nombre de ayuda no se guardo"
    assert_equal ayuda.descripcion, descripcion, "La descripcion de ayuda no se guardo"
    path = ayuda.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_img, path.at(1), "La imagen no se guardo"
    assert_equal ayuda.category_id, categoria, "La categoria no se guardo"
  end

  #Test US56 Delete como ayudar
  test "should destroy ayuda" do
    ayuda = ayudas(:ayuda1)
    ayuda_id = ayuda.id
    ayuda_id2 = ayudas(:ayuda2).id
    
    assert_difference('Ayuda.count', -1, "No se elimino la ayuda") do
      delete ayuda_url(ayuda)
    end
    
    assert_raises(Exception, "No se elimino la ayuda") do
      assert_not_nil Ayuda.find(ayuda_id)
    end
  end
  
end
