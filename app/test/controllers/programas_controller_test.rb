require 'test_helper'

class ProgramasControllerTest < ActionDispatch::IntegrationTest

  setup do
    @programa = programas(:one)
  end

  test "should get index" do
    get programas_url
    assert_response :success
  end

  test "should get new" do
    get new_programa_url
    assert_response :success
  end

#Test US43 Register social programs
  test "should create programa" do
    titulo="Programa prueba"
    descripcion="Descripcion del programa"
    assert_difference('Programa.count',1,"No se agrego el programa") do
      post programas_url, params: { programa: { descripcion: descripcion, titulo: titulo } }
    end

    programa = Programa.last
    assert_equal programa.titulo, titulo, "El titulo del programa no se guardo"
  end

#Test US46 and US47 Consult social programs
  test "should show programa" do
    programa = programas(:one)
    assert_raises(NameError, "El programa no esta registrado") do
      get programa_url(programa)
      assert_not_response :success
    end
  end

  test "should get edit" do
    get edit_programa_url(@programa)
    assert_response :success
  end

#Test US44 Update social programs
  test "should update programa" do
    programa = programas(:one)
    titulo="programa actualizado"
    #titulo_viejo = @programa.titulo
    descripcion="descripcion actualizada"
    patch programa_url(@programa), params: { programa: { descripcion: descripcion, titulo: titulo } }
    programa.reload
    assert_equal programa.titulo,titulo, "El titulo no se actualizo"
    assert_equal programa.descripcion,descripcion, "La descripcion no se actualizo"
  end

#Test US45 Delete social programs
  test "should destroy programa" do
    programa = programas(:one)
    programa_id= programa.id
    assert_difference('Programa.count', -1, "No se elimino el programa") do
      delete programa_url(programa)
    end

    assert_raises(Exception, "No se elimino el programa") do
      assert_not_nil Programa.find(programa_id)
    end
  end

end
