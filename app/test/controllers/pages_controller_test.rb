require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get nosotros" do
    get nosotros_path
    assert_response :success
  end

  test "should get programas" do
    get programas_path
    assert_response :success
  end

  test "should get eventos_noticias" do
    get eventos_noticias_path
    assert_response :success
  end


  test "should get como_ayudar" do
    get como_ayudar_path
    assert_response :success
  end

#Test US51 Consult the photo gallery
  test "should get galeria" do
    get galeria_path
    assert_response :success
  end
  
#Test US8 Consult the contact me
  test "should get contact form" do
    get new_contacto_url

    assert_response :success
    assert_select 'form' do 
      assert_select 'input[type=text]'
      assert_select 'input[type=text]'
      assert_select 'textarea'
      assert_select 'input[type=submit]'
    end
  end
end

