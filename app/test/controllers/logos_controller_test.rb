require 'test_helper'

class LogosControllerTest < ActionDispatch::IntegrationTest
=begin  
  setup do
    @logo = logos(:one)
  end

  test "should get index" do
    get logos_url
    assert_response :success
  end

  test "should get new" do
    get new_logo_url
    assert_response :success
  end

  test "should create logo" do
    assert_difference('Logo.count') do
      post logos_url, params: { logo: { imagen: @logo.imagen, nombre: @logo.nombre } }
    end

    assert_redirected_to logo_url(Logo.last)
  end

  test "should show logo" do
    get logo_url(@logo)
    assert_response :success
  end

  test "should get edit" do
    get edit_logo_url(@logo)
    assert_response :success
  end

  test "should update logo" do
    patch logo_url(@logo), params: { logo: { imagen: @logo.imagen, nombre: @logo.nombre } }
    assert_redirected_to logo_url(@logo)
  end

  test "should destroy logo" do
    assert_difference('Logo.count', -1) do
      delete logo_url(@logo)
    end

    assert_redirected_to logos_url
  end
=end

  test "Agregar imagen e nombre del logo" do
    nombre = "Certificacion 1"
    nombre_imagen = 'apple.png'
    imagen = fixture_file_upload nombre_imagen
    assert_difference('Logo.count', 1, "No se agrego el registro") do
      post logos_url, params: { logo: { nombre: nombre, imagen: imagen} }
    end
    nueva_foto = Logo.last
    path = nueva_foto.imagen.url(:thumb).split('thumb_')
    assert_equal nueva_foto.nombre, nombre, "El nombre del logo no se guardo"
    assert_equal nombre_imagen, path.at(1), "La foto no se subio correctamente"
  end
  
  test "should update logo" do
    logo = logos(:one)
    nombre = "Certificacion 2"
    nombre_img = 'apple.png'
    imagen = fixture_file_upload nombre_img
    patch logo_url(logo), params: { logo: { nombre: nombre, imagen: imagen } }
    logo.reload
    assert_equal logo.nombre, nombre, "El nombre del logo no se guardo"
    path = logo.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_img, path.at(1), "La imagen no se guardo"
  end

  test "deberia eliminar el logo" do
    logo = logos(:one)
    logo_id = logo.id
    logo_id2 = logos(:two).id
    assert_difference('Logo.count', -1, "No se elimino el logo") do
      delete logo_url(logo)
    end
    assert_raises(Exception, "No se elimino la foto del carrusel") do
      assert_not_nil Logo.find(logo_id)
    end
  end  

end
