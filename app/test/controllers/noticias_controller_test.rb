require 'test_helper'

class NoticiasControllerTest < ActionDispatch::IntegrationTest
 
  setup do
    @noticia = noticias(:one)
  end

  test "should create noticia" do
    nombre_imagen = 'noticia.jpg'
    imagen = fixture_file_upload nombre_imagen
    assert_difference('Noticia.count', 1, "No se creó la noticia") do
      post noticias_url, params: { noticia: { titulo: @noticia.titulo, descripcion: @noticia.descripcion, imagen: imagen } }
    end
    
    noticia = Noticia.last
    assert_equal noticia.titulo, @noticia.titulo, "El título de la noticia no se guardó"
    assert_equal noticia.descripcion, @noticia.descripcion, "La descripcion de la noticia no se guardó"
    image_path = noticia.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, image_path.at(1), "La imagen no se guardó"
  end
 
  test "should show noticia" do
    assert_raises(NameError, "La noticia no esta registrada") do
      get noticia_url(@noticia)
      assert_not_response :success
    end
  end

  test "should update noticia" do
    new_titulo = 'Título editado'
    new_descripcion = 'Descripción de noticia editada'
    nombre_imagen = 'noticia.jpg'
    imagen = fixture_file_upload nombre_imagen
    patch noticia_url(@noticia), params: { noticia: { titulo: new_titulo, descripcion: new_descripcion, imagen: imagen } }
    @noticia.reload
    assert_equal @noticia.titulo, new_titulo, "El título de la noticia no se actualizó"
    assert_equal @noticia.descripcion, new_descripcion, "La descripcion de la noticia no se actualizó"
    image_path = @noticia.imagen.url(:thumb).split('thumb_')
    assert_equal nombre_imagen, image_path.at(1), "La imagen no se guardó"
  end

  test "should destroy noticia" do
    noticia_id = @noticia.id
    #noticia_id_fail = noticias(:two).id
    
    assert_difference('Noticia.count', -1, "No se eliminó la noticia") do
      delete noticia_url(@noticia)
    end
    
    assert_raises(Exception, "No se eliminó la noticia") do
      assert_not_nil Noticia.find(noticia_id)
    end
  end

end
