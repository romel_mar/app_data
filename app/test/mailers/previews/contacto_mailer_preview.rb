# Preview all emails at http://localhost:3000/rails/mailers/contacto_mailer
class ContactoMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contacto_mailer/contactame
  def contactame    
    message = Contacto.new nombre: 'marflar', 
                          email: 'marflar@example.org',
                          descripcion: 'This is the body of the email'

    ContactoMailer.contactame message
  end

end
