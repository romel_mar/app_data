# Preview all emails at http://localhost:3000/rails/mailers/message_mailer
class MessageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/message_mailer/factura_yo
  def factura_yo 
    message = Factura.new nombre: 'romel',
                          apellidop: 'martinez',
                          apellidom: 'olvera',
                          email: 'romel@hotmail.com',
                          domicilio: 'pedregal',
                          razon_social: 'amor',
                          rfc: 'RMAO29WUIEO',
                          pais: 'México',
                          estado: 'Quéretaro',
                          municipio: 'Quéretaro',
                          fecha: '18/04/2018',
                          monto: '4000'

    FacturaMailer.factura_yo message
  end

end