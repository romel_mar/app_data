require 'test_helper'

class ContactoMailerTest < ActionMailer::TestCase

#Test US41 Contact the institution
  test "contactame" do
     message = Contacto.new nombre: 'Brenda',
                          email: 'brenda_fparada@hotmail.com',
                          descripcion: 'hello, how are you doing?'

    email = ContactoMailer.contactame(message)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal ['a01206527@itesm.mx'], email.to
    assert_equal ['brenda_fparada@hotmail.com'], email.from
  end

end
