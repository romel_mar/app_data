require 'test_helper'

class FacturaMailerTest < ActionMailer::TestCase
=begin  
  test "factura_yo" do
    factura = Factura.new nombre: 'romel',
                          apellidop: 'martinez',
                          apellidom: 'olvera',
                          email: 'romel@hotmail.com',
                          domicilio: 'pedregal',
                          razon_social: 'amor',
                          rfc: 'RMAO29WUIEO',
                          pais: 'México',
                          estado: 'Quéretaro',
                          municipio: 'Quéretaro',
                          fecha: '18/04/2018',
                          monto: '4000'

    email = FacturaMailer.factura_yo(factura)

    assert_emails 1 do
      email.deliver_now
    end

    #assert_equal 'Facturas economicas', email.subject
    assert_equal ['a01207360@itesm.mx'], email.to
    assert_equal ['romel@hotmail.com'], email.from
  end
=end
end
