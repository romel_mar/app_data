# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!([
  {email: "romel@hotmail.com", password: "contrasena", password_confirmation: "contrasena", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-02-06 14:02:10", last_sign_in_at: "2015-02-06 14:02:10", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", admin: "true", carrusel: "false", noticias: "false", nosotros: "false", fotos: "false", programas: "false", ayuda: "false", categoria: "false", logo: "false"},
  {email: "gil@mvmanor.co.uk", password: "contrasena", password_confirmation: "contrasena", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-02-06 14:03:01", last_sign_in_at: "2015-02-06 14:03:01", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", admin: "true", carrusel: "false", noticias: "false", nosotros: "false", fotos: "false", programas: "false", ayuda: "false", categoria: "false", logo: "false"},
  {email: "brenda@customer.co.uk", password: "contrasena", password_confirmation: "contrasena", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-02-06 14:03:44", last_sign_in_at: "2015-02-06 14:03:44", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", admin: "true", carrusel: "false", noticias: "false", nosotros: "false", fotos: "false", programas: "false", ayuda: "false", categoria: "false", logo: "false"},
  {email: "karina@customer.co.uk", password: "contrasena", password_confirmation: "contrasena", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-02-06 14:03:44", last_sign_in_at: "2015-02-06 14:03:44", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", admin: "true", carrusel: "false", noticias: "false", nosotros: "false", fotos: "false", programas: "false", ayuda: "false", categoria: "false", logo: "false"}
])