class CreateNoticias < ActiveRecord::Migration[5.1]
  def change
    create_table :noticias do |t|
      t.string :titulo
      t.text :descripcion
      t.string :imagen

      t.timestamps
    end
  end
end
