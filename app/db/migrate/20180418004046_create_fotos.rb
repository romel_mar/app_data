class CreateFotos < ActiveRecord::Migration[5.1]
  def change
    create_table :fotos do |t|
      t.string :nombre
      t.string :imagen
      t.integer :programa_id

      t.timestamps
    end
  end
end
