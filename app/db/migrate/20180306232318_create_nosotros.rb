class CreateNosotros < ActiveRecord::Migration[5.1]
  def change
    create_table :nosotros do |t|
      t.string :nombre
      t.text :descripcion
      t.string :archivos

      t.timestamps
    end
  end
end
