class DropProgramasTable < ActiveRecord::Migration[5.1]
  def up
    drop_table :programas
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
