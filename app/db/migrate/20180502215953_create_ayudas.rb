class CreateAyudas < ActiveRecord::Migration[5.1]
  def change
    create_table :ayudas do |t|
      t.string :nombre
      t.text :descripcion
      t.string :imagen
      t.integer :category_id
      
      t.timestamps
    end
  end
end
