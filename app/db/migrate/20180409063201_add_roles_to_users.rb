class AddRolesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :admin, :boolean, default: false
    add_column :users, :carrusel, :boolean, default: false
    add_column :users, :noticias, :boolean, default: false
    add_column :users, :nosotros, :boolean, default: false
    add_column :users, :fotos, :boolean, default: false
    add_column :users, :programas, :boolean, default: false
    add_column :users, :ayuda, :boolean, default: false
    add_column :users, :categoria, :boolean, default: false
    add_column :users, :logo, :boolean, default: false
  end
end
