class CreateGaleria < ActiveRecord::Migration[5.1]
  def change
    create_table :galeria do |t|
      t.integer :fecha
      t.string :imagen

      t.timestamps
    end
  end
end
