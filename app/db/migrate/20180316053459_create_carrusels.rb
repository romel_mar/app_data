class CreateCarrusels < ActiveRecord::Migration[5.1]
  def change
    create_table :carrusels do |t|
      t.string :imagen

      t.timestamps
    end
  end
end
