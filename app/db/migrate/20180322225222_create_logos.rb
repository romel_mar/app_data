class CreateLogos < ActiveRecord::Migration[5.1]
  def change
    create_table :logos do |t|
      t.string :nombre
      t.string :imagen

      t.timestamps
    end
  end
end
